FROM code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.11-alpine

RUN pip install requests

WORKDIR /usr/bin
COPY gitlab-ci/ci-script.py ci-script
RUN chmod +x ci-script

WORKDIR /openapi-browser
RUN wget --help
RUN wget -O index.js https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fopenapi-browser/packages/generic/umd/0.1.0/index.js
RUN wget -O styles.css https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fopenapi-browser/packages/generic/umd/0.1.0/styles.css
COPY gitlab-ci/index.html index.html
