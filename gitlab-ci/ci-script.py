#!/usr/bin/env python3

import json
import os

import requests


CI_API_V4_URL = os.getenv('CI_API_V4_URL')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID')
CI_JOB_TOKEN = os.getenv('CI_JOB_TOKEN')

r = requests.get(f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/packages?package_name=openapi",
                 headers={"JOB-TOKEN": CI_JOB_TOKEN})
try:
    r.raise_for_status()
except:
    raise Exception("Unable to load OpenAPI packages from Gitlab project registry")

data = r.json()

os.makedirs("public", exist_ok=True)

versions = {}
for package in data:
    version = package['version']
    openapi = requests.get(f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/packages/generic/openapi/{version}/openapi.json",
                           headers={"JOB-TOKEN": CI_JOB_TOKEN})
    openapi.raise_for_status()
    filename = f'{version}.openapi.json'
    with open(f'public/{filename}', 'w') as f:
        f.write(str(openapi.text))

    versions[version] = filename

with open("public/versions.js", 'w') as f:
    f.write(f"NMS_OPENAPI_VERSIONS = {str(json.dumps(versions))}")
