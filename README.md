# NMS API best practices


## Gitlab CI Pipelines
A few-lines Gitlab pipelines is provided as part of this repo to publish OpenAPI specs
in [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). This pipeline
will automatically search for OpenAPI specifications published in the Project registry
and publish a page to browse all version found

> For the pipeline to work, openapi `json` specifications must first be published
> as generic package
> ```yaml
> - publish-openapi-specs-generic-package-job:
>   variables:
>     VERSION: v0.1.0
>   script:
>     - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file build/openapi.json "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/openapi/${VERSION}/openapi.json"'
> ```

```yaml
include:
  - project: 'digit-c4/dev/api-best-practices'
    file: 'gitlab-ci/openapi-browser-pages-job.yml'
    ref: main

stages:
  - doc

# Job to generate pages must be named `pages`
pages:
  stage: doc
  tags:
    - docker
```
